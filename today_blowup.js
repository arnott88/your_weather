import React, { useState, useEffect } from 'react';
import App from './App.js'
import { Image, 
        ImageBackground,
        Dimensions,
        StyleSheet,
        Text, 
        View,  
        scrollView, 
        ScrollView,
        AsyncStorage,
        Alert,
        Picker,
        TouchableOpacity, 
        ShadowPropTypesIOS} from 'react-native';
 import { Button,
          LinearGradient, 
          Input, 
          Divider,
          ThemeProvider,
          Icon } from 'react-native-elements';
 import {
        LineChart,
        BarChart,
        PieChart,
        ProgressChart,
        ContributionGraph,
        StackedBarChart
        } from "react-native-chart-kit";
        import useStateWithCallback from "use-state-with-callback";
        import { icons, backgrounds } from './components/images.js'
        import SwiperFlatList from 'react-native-swiper-flatlist';
        import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
        import * as Font from 'expo-font';
        import GetCity from './location_finder.js';
        import { en, es } from './components/translations.js'
        import * as Localization from 'expo-localization';
        import i18n from 'i18n-js';
          
        i18n.fallbacks = true;
        i18n.translations = { es, en };
        i18n.locale = Localization.locale;

const Today = (props) => {
    const [weatherHour, setWeatherHour] = useStateWithCallback(null, weatherHour => {
        if(weatherHour !== null && count < 1) {
            setCount(count + 1)
            getData()
            setDisplay(true)
            console.log('display ====', display, icons[weatherHour.list[0].weather[0].icon])
            
            
        }
    })
    const [display, setDisplay] = useStateWithCallback(false, weatherHour => {
        if(weatherHour !== null) {
            setDisplay(true)
        }
    })
    const [count, setCount] = useState(0)
    const [hours, setHours] = useStateWithCallback(null, hours => {
        if(hours !== null) {
            console.log('inside get hours child function', hours)
            data.labels = []
            hours.forEach((hour, i) => {
                if(data.labels.length < 8) {
                    data.labels.push(hour)
                }
            })
            setDisplay(true)
        }
    })
    const [temps, setTemps] = useStateWithCallback(null, temps =>{
        if(temps !== null) {
            console.log('inside child get temps state', temps)
            data.datasets[0].data = []
            temps.forEach((temp, i) => {
                if(data.datasets[0].data.length < 8) {
                    data.datasets[0].data.push(temp)
                }
            })
            setDisplay(true)
        }
    })
    const [data, setData] = useState({
                                        labels: [9, 10, 11],
                                        datasets: [
                                        {
                                            data: [20, 45, 28, 80, 99, 43],
                                            fontFamily: 'neucha',
                                            color: props.dayNight === 'day' ? (opacity = 1) => `rgba(255,186,85, ${opacity})` : (opacity = 1) => `rgba(255,186,85, ${opacity})`,
                                            strokeWidth: 2, // optional,
                                        }
                                        ]
                                    })
  
//===================================FUNCTIONS===========================================
var font = ''
    useEffect(() => {
    font = Font.loadAsync({'neucha': require('./assets/fonts/Neucha-Regular.ttf')})
        setWeatherHour(props.hourlyWeather)
    },[props.hourlyWeather])

    const getData = () => {
        tempLables = []
        tempData = []
        weatherHour.list.forEach((list, i) => {
            let time = new Date((list.dt + weatherHour.city.timezone) * 1000).toString()
            let t = time.split(' ')[4]
            let e = t.substring(0,5)    
            if(e === '00:00') {
                tempLables.push('12 AM')
            }
            else if(e === '01:00') {
                tempLables.push('1 AM')
            }
            else if(e === '02:00') {
                tempLables.push('2 AM')
            }
            else if(e === '03:00') {
                tempLables.push('3 AM')
            }
            else if(e === '04:00') {
                tempLables.push('4 AM')
            }
            else if(e === '05:00') {
                tempLables.push('5 AM')
            }
            else if(e === '06:00') {
                tempLables.push('6 AM')
            }
            else if(e === '07:00') {
                tempLables.push('7 AM')
            }
            else if(e === '08:00') {
                tempLables.push('8 AM')
            }
            else if(e === '09:00') {
                tempLables.push('9 AM')
            }
            else if(e === '10:00') {
                tempLables.push('10 AM')
            }
            else if(e === '11:00') {
                tempLables.push('11 AM')
            }
            else if(e === '12:00') {
                tempLables.push('12 PM')
            }
            else if(e === '13:00') {
                tempLables.push('1 PM')
            }
            else if(e === '14:00') {
                tempLables.push('2 PM')
            }
            else if(e === '15:00') {
                tempLables.push('3 PM')
            }
            else if(e === '16:00') {
                tempLables.push('4 PM')
            }
            else if(e === '17:00') {
                tempLables.push('5 PM')
            }
            else if(e === '18:00') {
                tempLables.push('6 PM')
            }
            else if(e === '19:00') {
                tempLables.push('7 PM')
            }
            else if(e === '20:00') {
                tempLables.push('8 PM')
            }
            else if(e === '21:00') {
                tempLables.push('9 PM')
            }
            else if(e === '22:00') {
                tempLables.push('10 PM')
            }
            else if(e === '23:00') {
                tempLables.push('11 PM')
            }
            
            tempData.push(Math.round(list.main.temp))
        })
        setHours([...tempLables])
        setTemps([...tempData])
        console.log('inside get hours child function', hours)
    }
    
    const chartConfig = {
        decimalPlaces: 0,
        fontFamily: 'neucha',
        fillShadowGradient: '#6ebcbf',
        fillShadowGradientOpacity: props.dayNight === 'day' ? .3 : .6,
        backgroundGradientFrom: "#e69775",
        backgroundGradientFromOpacity: 0,
        backgroundGradientTo: "#08130D",
        backgroundGradientToOpacity: 0,
        color: props.dayNight === 'day' ? (opacity = 1) => `rgba(0, 0, 0, ${opacity})` : (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
        strokeWidth: 0, // optional, default 3
        barPercentage: 4,
        propsForDots: {
            r: 0,
            strokeWidth: 0,
            stroke: "#e69775"
        },
      };

  return (
    <View style={styles.container}>
        <View style={styles.today}>
        <Text style={{alignSelf: 'center', fontFamily: 'neucha', fontSize:RFPercentage(7), color: props.dayNight === 'day' ? 'black' : '#fff'}}>{props.currentWeather.name}</Text>
            <Text style={{fontFamily: 'neucha', fontSize: RFPercentage(5), color: props.dayNight === 'day' ? 'black' : '#fff'}}>{props.currentDay}</Text>
            <View style={styles.temps}>
            <View style={{flexDirection: 'column', width: '50%', borderRightWidth: 3, borderColor: props.dayNight === 'day' ?  'rgba(0, 0, 0, .1)' : 'rgba(255, 255, 255, .2)'}}>
                <Text style={{fontFamily: 'neucha', alignSelf: 'center', borderColor: props.dayNight === 'day' ?  'rgba(0, 0, 0, .1)' : 'rgba(255, 255, 255, .2)',fontSize: RFPercentage(5), color: props.dayNight === 'day' ? 'black' : '#fff'}}>{`Min: ${Math.round(props.currentWeather.main.temp_min * 10) / 10}°`}</Text>
                <Text style={{fontFamily: 'neucha', alignSelf: 'center', borderTopWidth: 3,fontSize: RFPercentage(5), color: props.dayNight === 'day' ? 'black' : '#fff', borderColor: props.dayNight === 'day' ?  'rgba(0, 0, 0, .1)' : 'rgba(255, 255, 255, .2)'}}>{`Max: ${Math.round(props.currentWeather.main.temp_max * 10) / 10}°`}</Text>
            </View>
            <View style={{flexDirection: 'column', width: '50%'}}>
            <Text style={{fontFamily: 'neucha', alignSelf: 'center',paddingLeft: 5,borderBottomWidth: 3,fontSize: RFPercentage(3.7), color: props.dayNight === 'day' ? 'black' : '#fff', borderColor: props.dayNight === 'day' ?  'rgba(0, 0, 0, .1)' : 'rgba(255, 255, 255, .2)'}}>{`${i18n.t('Wind')}: ${props.currentWeather.wind.speed} km/h`}</Text>
                <Text style={{fontFamily: 'neucha', alignSelf: 'center',paddingLeft: 5,borderBottomWidth: 3,fontSize: RFPercentage(3.7), color: props.dayNight === 'day' ? 'black' : '#fff', borderColor: props.dayNight === 'day' ?  'rgba(0, 0, 0, .1)' : 'rgba(255, 255, 255, .2)'}}>{`${i18n.t('Humidity')}: ${props.currentWeather.main.humidity}%`}</Text>
                <Text style={{fontFamily: 'neucha', alignSelf: 'center',fontSize: RFPercentage(3.6), color: props.dayNight === 'day' ? 'black' : '#fff'}}>{`${i18n.t('Feels like')}: ${Math.round(props.currentWeather.main.feels_like * 10) / 10}°`}</Text>
                
            </View>
            </View>
        </View>
        <View>
        <ScrollView horizontal={true}>  
        <View style={styles.chart}>
            <LineChart
                data={data}
                width={screenWidth}
                height={230}
                verticalLabelRotation={0}
                chartConfig={chartConfig}
                withInnerLines={false}
                withOuterLines={false}
                yAxisSuffix={'°'}
                bezier
            />
        </View> 
     
        </ScrollView>
        <View style={{flexDirection: 'row', height: '70%', paddingBottom: '22%',alignItems: 'center', justifyContent: 'space-around', paddingLeft: '12%', paddingRight: '5%'}}>
               {display
               ? 
               data.labels.map((hour, i) => <View style={{width: '8%'}}>
                                                    <View style={{flex: 1, flexDirection: 'column'}}>
                                                     
                                                        <Image style={{height: weatherHour.list[i].weather[0].main === 'Rain' 
                                                        || weatherHour.list[i].weather[0].main === 'Drizzle'
                                                        || weatherHour.list[i].weather[0].main === 'Thunderstorm' 
                                                        || weatherHour.list[i].weather[0].main === 'Snow'   ? '6%' : '10%', width: '100%'}} source={icons[weatherHour.list[i].weather[0].icon]}/>
                                                    </View>
                                                  </View>
               )
                : null}
        </View>
        </View>
    </View>
  );
}
const screenWidth = Dimensions.get("window").width;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  today: {
      height: '40%',
      width: screenWidth,
      alignItems: 'center',
      justifyContent: 'space-evenly'
  },
  chart: {
      height: '60%',
      marginBottom: '10%',
  },
  temps: {
      height: '40%',
      flexDirection: 'row',
      width: width,
      paddingLeft: '5%',
      paddingRight: '5%',
      alignItems: 'center'
  }

});
export const { width, height } = Dimensions.get('window');
export default Today