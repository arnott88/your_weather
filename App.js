import React, { useState, useEffect, useCallback } from 'react';
import { Image, 
        Dimensions,
        AppRegistry,
        ImageBackground,
        StyleSheet,
        Text, 
        View,  
        scrollView, 
        ScrollView,
        AsyncStorage,
        Alert,
        Picker,
        TouchableOpacity,
        I18nManager,
        SafeAreaView} from 'react-native';
import { Button,
          LinearGradient, 
          Input, 
          Divider,
          ThemeProvider,
          Icon } from 'react-native-elements';
import {
          LineChart,
          BarChart,
          PieChart,
          ProgressChart,
          ContributionGraph,
          StackedBarChart
        } from "react-native-chart-kit";
import useStateWithCallback from "use-state-with-callback";
import { icons, backgrounds } from './components/images.js';
import { en, es } from './components/translations.js'
import Today from './today_blowup.js'
import GetCity from './location_finder.js'
import Axios from 'axios' 
import SwiperFlatList from 'react-native-swiper-flatlist';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import * as Font from 'expo-font';
import {owm_API} from './config.js'
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';


i18n.fallbacks = true;
i18n.translations = { es, en };
i18n.locale = Localization.locale;

export default function App() {



 const [backgroundLoader, setBackgroundLoader] = useState('')
 const [iconLoader, setIconLoader] = useState('')
 const [rain, setRain] = useState(false)
 const [currentDay, setCurrentDay] = useState('')
 const [dayNight, setDayNight] = useState('day')
 const [displayFinder, setDisplayFinder] = useState(false)
 const [lat, setLat] = useState(null)
 const [lon, setLon] = useState(null)
 const [count, setCount] = useState(0)
 const [position, setPosition] = useStateWithCallback(null, position => {
   console.log('count =', count)
   if(position !== null && count !== 1) {
    setCount(count + 1)
    setCount2(0)
    setCount3(0)
    setCount4(0)
    getCurrentWeather()
    console.log('callback===================complete', position)
   }
 })
 const [count2, setCount2] = useState(0)
 const [currentWeather, setCurrentWeather] = useStateWithCallback(null, currentWeather => {
   console.log('count2', count2)
   if(currentWeather !== null && count2 !== 1) {
    console.log('inside callback currentWeather', currentWeather)
    setCount2(count2 + 1)
    getHourlyWeather()
    dayNightFunc()
    getRain()
    setTimeout(() => {setLoader(false)}, 2000)
    
   }
 })
 const [count3, setCount3] = useState(0)
 const [hourlyWeather, setHourlyWeather] = useStateWithCallback(null, hourlyWeather => {
   if(hourlyWeather !== null && count3 !== 1) {
    console.log('inside callback hourlyWeather', hourlyWeather)
     setCount3( count3 + 1)
     getWeeklyWeather()
    //  dayNightFunc()
   }
 })
 const [count4, setCount4] = useState(0)
 const [weeklyWeather, setWeeklyWeather] = useStateWithCallback(null, weeklyWeather => {
  if(weeklyWeather !== null && count4 !== 1) {
    setCount4(count4 + 1)
    getDays()
    
    // dayNightFunc()
    console.log('firat days', weeklyWeather.list[0].dt)
  }
 })
 const [displayDays, setDisplayDays] = useStateWithCallback(false, displayDays => {
   if(days !== null) {
     setDisplayDays(true)
    
     
   }
 })
 const [days, setDays] = useStateWithCallback(null, days => {
   if(days !== null) {
    getToday()
     console.log('days are not null', days)  
   }
 })
 const [loader, setLoader] = useState(true)

//======================FUNCTIONS==========================================================

useEffect(() => {
  Font.loadAsync({'neucha': require('./assets/fonts/Neucha-Regular.ttf')})
  currentLoc()
  console.log('useEffect', position)
},[])
useEffect(() => {
  imageDisplayer()
},[loader])


const currentLoc = () => {
  resetWeather()
  setDisplayFinder(false)
  setCount(0)
  navigator.geolocation.getCurrentPosition( realPosition => {
      console.log('this is the coords', realPosition)
      setPosition({...realPosition})
      console.log('coords============>', position)
      },  
      error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      )
    }

const getRain = () => {
  setRain(false)
  if (currentWeather.weather[0].main === 'Rain' || currentWeather.weather[0].main === 'Drizzle'
   || currentWeather.weather[0].main === 'Thunderstorm' || currentWeather.weather[0].main === 'Snow') {
    setRain(true)
  }
}

const getCurrentWeather = async () => {
  let lat
  let lon
  let key = owm_API
  let count = 7
  
  if(position !== null && position.coords) {
    lat = position.coords.latitude
    lon = position.coords.longitude
    try {
        const response= await Axios.post(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&APPID=${key}`)
        setCurrentWeather({...response.data})
        console.log('current weather good=====>', currentWeather)
      }   
      catch(error) 
    {
      console.log('current weather bad', error, currentWeather)
    }
  }
  else if(position !== null && position.location) {
    lat = position.location.lat
    lon = position.location.lng
    try {
        const response= await Axios.post(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&APPID=${key}`)
        setCurrentWeather({...response.data})
        console.log('current city weather good=====>', currentWeather)
      }   
      catch(error) 
    {
      console.log('current city weather bad', error, currentWeather)
    }
  }
}
const getHourlyWeather = async () => {
  let lat
  let lon
  let key = owm_API
  let count = 7
  if(position !== null && position.coords) {
    lat = position.coords.latitude
    lon = position.coords.longitude
    try {
        const response1= await Axios.post(`http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&units=metric&APPID=${key}`)
        setHourlyWeather({...response1.data})
        console.log('hourly weather good=====>', hourlyWeather)
      }   
      catch(error) 
    {
      console.log('hourly weather bad=====>', hourlyWeather)
    }
  }
  else if(position !== null && position.location) {
    lat = position.location.lat
    lon = position.location.lng
    try {
      const response1= await Axios.post(`http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&units=metric&APPID=${key}`)
        setHourlyWeather({...response1.data})
        console.log('current weather good=====>', currentWeather)
      }   
      catch(error) 
    {
      console.log('current weather bad', error, currentWeather)
    }
  }
}
const getWeeklyWeather = async () => {
  let lat
  let lon
  let key = owm_API
  let count = 7
  if(position !== null && position.coords) {
    lat = position.coords.latitude
    lon = position.coords.longitude
    try {
        const response2= await Axios.post(`http://api.openweathermap.org/data/2.5/forecast/daily?lat=${lat}&lon=${lon}&cnt=${count}&units=metric&APPID=${key}`)
        setWeeklyWeather({...response2.data})
        console.log('weekly weather good=====>', weeklyWeather)
      }   
      catch(error) 
    {
      console.log('weekly weather bad', error, weeklyWeather)
    }
  }
  else if(position !== null && position.location) {
    lat = position.location.lat
    lon = position.location.lng
    try {
      const response2= await Axios.post(`http://api.openweathermap.org/data/2.5/forecast/daily?lat=${lat}&lon=${lon}&cnt=${count}&units=metric&APPID=${key}`)
        setWeeklyWeather({...response2.data})
        console.log('current weather good=====>', currentWeather)
      }   
      catch(error) 
    {
      console.log('current weather bad', error, currentWeather)
    }
  }
}
const getDays = () => {
  let tempDays = []
  let timeshift = weeklyWeather.city.timezone
  weeklyWeather.list.forEach((day, i) => {
    let tempDay
    let tempIcon
    let tempTemp
    let date = new Date((day.dt + timeshift)*1000).toString()
    tempDay = date.split(' ')[0]
    tempIcon = icons[weeklyWeather.list[i].weather[0].icon]
    tempTemp = day.temp.max
    if(tempDays.length < 6) {
      tempDays.push({name: tempDay, icon: tempIcon, temp: tempTemp})
    }
    console.log('new days', tempDays)
  })
  tempDays.shift()
  setDays([...tempDays])
  
}
const getToday = () => {
  // let num = currentWeather.dt + currentWeather.timezone
  // let temp = new Date(num)
  // console.log('currentWeather.dt', temp)
  // let today =  temp.getDay()
  if(days[0].name === 'Mon') {
    setCurrentDay(i18n.t('Sunday'))
  }
  else if(days[0].name === 'Tue') {
    setCurrentDay(i18n.t('Monday'))
  }
  else if(days[0].name === 'Wed') {
    setCurrentDay(i18n.t('Tuesday'))
  }
  else if(days[0].name === 'Thu') {
    setCurrentDay(i18n.t('Wednesday'))
  }
  else if(days[0].name === 'Fri') {
    setCurrentDay(i18n.t('Thursday'))
  }
  else if(days[0].name === 'Sat') {
    setCurrentDay(i18n.t('Friday'))
  }
  else if(days[0].name === 'Sun') {
    setCurrentDay(i18n.t('Saturday'))
  }
  
  console.log('today', currentDay)
}
const getCityFunc = () => {
  if(displayFinder === false) {
    setDisplayFinder(true)
  } else {
    setDisplayFinder(false)
  }
  console.log('get city')
}
const dayNightFunc = () => {
  if(currentWeather.weather[0].icon.charAt(2) === 'n'){
      setDayNight('night')
  } else {
    setDayNight('day')
  }
}
const resetWeather = () => {
  setLoader(true)
  setPosition(null)
  setCurrentWeather(null)
  setHourlyWeather(null)
  setWeeklyWeather(null)
  setDisplayDays(false)
}
const imageDisplayer = () => {
  let num = Math.floor((Math.random() * 7) + 1)
  let num2 = Math.floor((Math.random() * 3) + 1)
  let bg = ''
  let icon = ''
  if(num === 1) {
    bg = backgrounds['01d']
  }
  else if(num === 2) {
    bg = backgrounds['13d']
  }
  else if(num === 3) {
    bg = backgrounds['13n']
  }
  else if(num === 4) {
    bg = backgrounds['13n']
  }
  else if(num === 5) {
    bg = backgrounds['16n']
  }
  else if(num === 6) {
    bg = backgrounds['01d']
  }
  else if(num === 7) {
    bg = backgrounds['lp']
  }
  if(bg === backgrounds['01d']) {
    if(num2 === 1) {
      icon = icons['01d']
    }
    else if(num2 === 2) {
      icon = icons['03d']
    }
    else if(num2 === 3) {
      icon = icons['02d']
    }
  }
  else if(bg === backgrounds['13d']) {
    if(num2 === 1) {
      icon = icons['01d']
    }
    else if(num2 === 2) {
      icon = icons['13d']
    }
    else if(num2 === 3) {
      icon = icons['13n']
    }
  }
  else if(bg === backgrounds['lp']) {
    icon === null
  } else if(bg === backgrounds['16n']) {
    icon === null
  } else {
    if(num2 === 1) {
      icon = icons['01n']
    }
    else if(num2 === 2) {
      icon = icons['02n']
    }
    else if(num2 === 3) {
      icon = icons['03n']
    }
  }
  setBackgroundLoader(bg)
  setIconLoader(icon)
}
useEffect(() => {
  console.log('current weather!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', currentWeather, hourlyWeather, weeklyWeather)
},[weeklyWeather])
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;
  return !loader 
         ? <ImageBackground style={{height: '100%', width: screenWidth}} source={backgrounds[currentWeather.weather[0].icon]}>
           <View style={styles.header}>
                  <TouchableOpacity onPress={() => currentLoc()}>
                    <Image style={styles.loc} source={require('./assets/location.png')}/>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => getCityFunc()}>
                  <Image style={styles.loc} source={require('./assets/global.png')}/>
                  </TouchableOpacity>
                </View>
           {displayFinder
                ?
                <View style={{ height: '88%', width, zIndex: 9999}}>
                    <GetCity position={position} setPosition={setPosition} setDisplayFinder={setDisplayFinder} rain={rain}
                    dayNight={dayNight} setCount={setCount} setLoader={setLoader} setDisplayDays={setDisplayDays} resetWeather={resetWeather}/>
                  </View> :null}


         <SwiperFlatList
              index={0}
              showPagination
            >
            <View>

              <View style={styles.container}>
                <View style={styles.box}>
                <View style={styles.main}>
                  
                  <Text style={{fontFamily: 'neucha',fontSize: RFPercentage(7), paddingBottom: 20, color:dayNight==='day'?'#000' : '#fff'}}>{currentWeather.name}</Text> 
                  <Text style={{fontFamily: 'neucha', fontSize: RFPercentage(5), color:dayNight==='day'?'#000' : '#fff'}}>{currentDay}</Text>
                  <Image style={{height: rain ? '70%' : '100%', width: rain ? '70%' : '100%', marginBottom: rain ? '-8%' : '-25%', marginTop:rain ? '-17%' : '-35%'}} source={icons[currentWeather.weather[0].icon]}/>
                  <Text style={{fontFamily: 'neucha', fontSize: RFPercentage(8), color:dayNight==='day'?'#000' : '#fff'}}>{Math.round(currentWeather.main.temp * 10) / 10}°</Text>
                </View>
                
                <View style={styles.days}>
                  {displayDays
                  ? 
                  days.map((ele, i) => <View style={styles.day}>
                                          <Text style={{fontSize: RFPercentage(3),fontFamily: 'neucha', color:dayNight==='day'?'#000' : '#fff'}}>{i18n.t(ele.name)}</Text>
                                          <Image style={{height: '40%', width: '100%', color:dayNight==='day'?'#000' : '#fff'}} source={ele.icon}/>
                                          <Text style={{fontSize: RFPercentage(3),fontFamily: 'neucha', color:dayNight==='day'?'#000' : '#fff'}}>{Math.round(ele.temp)}°</Text>
                                        </View>
                    )
                  
                  : null}
                </View>
                </View>
              </View>
              
              </View>

                <View>
                  <Today currentDay={currentDay} hourlyWeather={hourlyWeather} dayNight={dayNight} currentWeather={currentWeather}/>
                  </View>
              </SwiperFlatList>
              </ImageBackground>
  : 
      <ImageBackground style={{height:'100%', width: screenWidth}} source={backgroundLoader}>
        <Image style={{height: 400, width: 400, alignSelf: 'center'}} source={iconLoader}/>
      </ImageBackground>
    
}
export const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width
  },
  header: {
    flexDirection: "row",
    justifyContent: 'space-between',
    paddingTop: 40,
    marginLeft: 20,
    marginRight: 20,
    paddingBottom: 20
  },
  loc: {
    justifyContent: "flex-start",
    height: 30,
    width: 30,
  },
  city: {
    justifyContent: "flex-end",
    height: 30,
    width: 30,
  },
  box: {
    flex: 1,
  },
  main: {
    height: '65%',
    flexDirection: 'column',
   
    alignItems: 'center',
    justifyContent: 'center',
  },
  days: {
    height: '25%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: 5,
    marginRight: 5,
  },
  day: {
    width: '20%',
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: 25,
    justifyContent: 'space-around',

  }
});
AppRegistry.registerComponent('myproject', () => Swiper);
// current api.openweathermap.org/data/2.5/weather?lat={41.391096999999995}&lon={2.1548569}

// by geo pro.openweathermap.org/data/2.5/forecast/hourly?lat=35&lon=139

// by location pro.openweathermap.org/data/2.5/forecast/hourly?id=524901

//API key 16909a97489bed275d13dbdea4e01f59

//temp key 540c83a37287a0f701cccc3256f36337

//maps API key: AIzaSyCriIeKw51AmyuU-diu2esuiQyf6iYXeYA

//rain ? -20 : -75, marginTop:rain ? -50 : -100