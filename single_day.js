import React, { useState, useEffect, useCallback } from 'react';
import { Image, 
        ImageBackground,
        StyleSheet,
        Text, 
        View,  
        scrollView, 
        ScrollView,
        AsyncStorage,
        Alert,
        Picker,
        TouchableOpacity, 
        ShadowPropTypesIOS} from 'react-native';
import { Button,
          LinearGradient, 
          Input, 
          Divider,
          Icon } from 'react-native-elements';
import {
          LineChart,
          BarChart,
          PieChart,
          ProgressChart,
          ContributionGraph,
          StackedBarChart
        } from "react-native-chart-kit";
import useStateWithCallback from "use-state-with-callback";
import { icons, backgrounds } from './components/images.js'

// import Today from './today_blowup.js'
import Axios from 'axios' 
import SingleDay from './single_day.js'
import { element } from '../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/@types/prop-types/index.js';

export default function App() {

return (
    
    <View>
        <Text>{props.ele.name}</Text>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  days: {
    flex:3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: 5,
    marginRight: 5,
  },
  day: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: 20,
    justifyContent: 'space-around',

  }
});
