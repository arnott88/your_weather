import React, { useState, useEffect } from 'react';
import App from './App.js'
import { Image, 
        ImageBackground,
        Dimensions,
        StyleSheet,
        Text, 
        View,  
        scrollView, 
        ScrollView,
        AsyncStorage,
        Alert,
        Picker,
        TouchableOpacity, 
        ShadowPropTypesIOS} from 'react-native';
 import { Button,
          LinearGradient, 
          Input, 
          Divider,
          ThemeProvider,
          Icon } from 'react-native-elements';
        import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
        import { icons, backgrounds } from './components/images.js'
        import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
        import { en, es } from './components/translations.js'
import { setConfigurationAsync } from 'expo/build/AR';
import {googlePl_API} from './config.js'
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
          

i18n.fallbacks = true;
i18n.translations = { es, en };
i18n.locale = Localization.locale;

const GetCity = (props) => {

  
//===================================FUNCTIONS===========================================


  return (
    <GooglePlacesAutocomplete
      placeholder= {i18n.t('Search for location:')}
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'default'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      listViewDisplayed='auto'    // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details = null) => {
        props.resetWeather()
        // props.setDisplayDays(false)
        props.setLoader(true)
       // 'details' is provided when fetchDetails = true
        console.log('return from getCity' , details);
        props.setPosition(null)
        props.setCount(0)
        props.setPosition({...details.geometry})
        
        props.setDisplayFinder(false)
        console.log('getCity================', details.geometry.location)
      }}
      getDefaultValue={() => ''}
      
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: googlePl_API,
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}

      styles={{
        textInputContainer: {
          backgroundColor: 'rgba(0,0,0,0.1)',
          borderTopWidth: 0,
          borderBottomWidth: 0,
          height: '8%',
          fontFamily: 'neucha'
        },
        textInput: {
          marginLeft: '5%',
          marginRight: '5%',
          height: '73%',
          color: 'black',
          fontSize: RFPercentage(3),
          fontFamily: 'neucha'
        },
        predefinedPlacesDescription: {
          color: props.dayNight === 'day' ? 'black' : '#fff',
          fontFamily: 'neucha'
        },
        description: {
            color: props.dayNight === 'day' ? 'black' : '#fff',
            fontFamily: 'neucha',
            fontSize: RFPercentage(2.5),
        },
        poweredContainer: {
            backgroundColor: 'rgba(52, 52, 52, 0.1)',
            color: props.dayNight === 'day' ? '#5d5d5d' : '#fff',
            fontFamily: 'neucha'
        }, row: {
          height: '100%'
        }
      }}
    />
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 10
  },

});
export const { width, height } = Dimensions.get('window');
export default GetCity